const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const connectionString = process.env.MONGO_DB_URI

// conexion a mongodb
mongoose.connect(connectionString, {
  useNewUrlParser: true,
  UseUnifiedTopology: true
})

const conn = mongoose.connection

conn.on('error', () => console.error.bind(console, 'connection error'))

conn.once('open', () => console.info('Connection to Database is successful'))

module.exports = conn
