require('../models/License')

const Driver = require('../models/Driver')
const Manager = require('../models/Manager')
const Car = require('../models/Car')

const bcrypt = require('bcrypt')
const { guardIsItemOfManager } = require('../common/guards')
const transaction = require('../common/transaction')
const {
  findArrayAndAdd,
  findArrayAndRemove,
  findManagerAndRemove,
  findManagerAndAdd
} = require('../common/findAndChange')

const find = (request, response, next) => {
  const { userId } = request
  const filter = { manager: userId }
  Driver.find({ filter }).populate('cars').then(users => {
    response.json(users)
  }).catch(err => {
    next(err)
  })
}

const findOne = (request, response, next) => {
  const { id } = request.params
  const { userId } = request
  Driver.findById(id).populate('cars').then(driver => {
    if (driver) {
      guardIsItemOfManager(userId, driver)
      return response.json(driver)
    }
    return response.status(404).end()
  }).catch(err => {
    next(err)
  })
}

const create = async (request, response, next) => {
  const { dni, password, name, surnames, gender, phone, percentageProfit, baseSalaryPerDay, driverLicense, socialSecurityNumber, cars } = request.body
  const { userId } = request

  const manager = await Manager.findById(userId)

  const saltRounds = 10
  const passwordHash = await bcrypt.hash(password, saltRounds)

  const newDriver = new Driver({
    dni,
    passwordHash,
    name,
    surnames,
    gender,
    phone,
    percentageProfit,
    baseSalaryPerDay,
    driverLicense,
    socialSecurityNumber,
    cars,
    manager: manager.id
  })

  transaction(request, response, next, async (session) => {
    try {
      const savedDriver = await newDriver.save({ session })
      await findManagerAndAdd(manager, 'drivers', savedDriver.id, session)
      await findArrayAndAdd(cars, 'drivers', savedDriver.id, Car, session)
      return Promise.resolve(savedDriver)
    } catch (err) {
      return Promise.reject(err)
    }
  })
}

const update = async (request, response, next) => {
  const { id } = request.params

  const { dni, password, name, surnames, gender, phone, percentageProfit, baseSalaryPerDay, driverLicense, socialSecurityNumber } = request.body
  const { userId } = request

  const manager = await Manager.findById(userId)

  const saltRounds = 10
  const passwordHash = await bcrypt.hash(password, saltRounds)

  const newDriver = new Driver({
    dni,
    passwordHash,
    name,
    surnames,
    gender,
    phone,
    percentageProfit,
    baseSalaryPerDay,
    driverLicense,
    socialSecurityNumber,
    manager: manager.id
  })
  transaction(request, response, next, async (session) => {
    try {
      const result = await Driver.findByIdAndUpdate(id, newDriver, { new: true })
      if (result) {
        guardIsItemOfManager(userId, result)
      }
      return Promise.resolve(result)
    } catch (err) {
      return Promise.reject(err)
    }
  })
}

const remove = async (request, response, next) => {
  const { id } = request.params
  const { userId } = request

  transaction(request, response, next, async (session) => {
    try {
      const result = await Driver.findByIdAndRemove(id, { session })
      if (result) {
        guardIsItemOfManager(userId, result)
      }
      await findManagerAndRemove(result.manager, 'drivers', id, Manager, session)
      await findArrayAndRemove(result?.cars, 'drivers', id, Car, session)
      return Promise.resolve(result)
    } catch (err) {
      return Promise.reject(err)
    }
  }, true)
}

module.exports = {
  find,
  findOne,
  create,
  update,
  remove
}
