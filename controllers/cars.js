/* eslint-disable no-unused-vars */

const Car = require('../models/Car')
const Manager = require('../models/Manager')
const License = require('../models/License')
const Driver = require('../models/Driver')

const { guardIsItemOfManager } = require('../common/guards')
const transaction = require('../common/transaction')
const {
  findItemAndChange,
  findArrayAndAdd,
  findItemAndRemove,
  findArrayAndRemove,
  findManagerAndRemove,
  findManagerAndAdd
} = require('../common/findAndChange')

const find = (request, response, next) => {
  const { userId } = request
  const filter = { manager: userId }
  Car.find(filter).populate(['license', 'drivers']).then(cars => {
    response.json(cars)
  }).catch(err => {
    next(err)
  })
}

const findOne = (request, response, next) => {
  const { id } = request.params
  const { userId } = request
  Car.findById(id).then(car => {
    if (car) {
      guardIsItemOfManager(userId, car)
      return response.json(car)
    }
    return response.status(404).end()
  }).catch(err => {
    next(err)
  })
}

const create = async (request, response, next) => {
  const { vehicleRegistration, brand, model, registrationDate, fuelType, vin, insurancePolicy, license, drivers } = request.body
  const { userId } = request

  const manager = await Manager.findById(userId)

  const newCar = new Car({
    vehicleRegistration,
    brand,
    model,
    registrationDate,
    fuelType,
    vin,
    insurancePolicy,
    license,
    drivers,
    manager: manager.id
  })

  transaction(request, response, next, async (session) => {
    try {
      const savedCar = await newCar.save({ session })
      await findManagerAndAdd(manager, 'cars', savedCar.id, session)
      await findItemAndChange(license, 'car', savedCar.id, License, session)
      await findArrayAndAdd(drivers, 'cars', savedCar.id, Driver, session)
      return Promise.resolve(savedCar)
    } catch (err) {
      return Promise.reject(err)
    }
  })
}

const update = async (request, response, next) => {
  const { id } = request.params
  const { vehicleRegistration, brand, model, registrationDate, fuelType, vin, insurancePolicy } = request.body
  const { userId } = request
  const newCar = new Car({
    vehicleRegistration,
    brand,
    model,
    registrationDate,
    fuelType,
    vin,
    insurancePolicy,
    manager: userId
  })

  transaction(request, response, next, async (session) => {
    try {
      const result = await Car.findByIdAndUpdate(id, newCar, { new: true })
      if (result) {
        guardIsItemOfManager(userId, result)
      }
      return Promise.resolve(result)
    } catch (err) {
      return Promise.reject(err)
    }
  })
}

const remove = async (request, response, next) => {
  const { id } = request.params
  const { userId } = request
  transaction(request, response, next, async (session) => {
    try {
      const result = await Car.findByIdAndRemove(id, { session })
      if (result) {
        guardIsItemOfManager(userId, result)
      }
      await findManagerAndRemove(result.manager, 'cars', id, Manager, session)
      await findItemAndRemove(result?.license, 'car', License, session)
      await findArrayAndRemove(result?.drivers, 'cars', id, Driver, session)
      return Promise.resolve(result)
    } catch (err) {
      return Promise.reject(err)
    }
  }, true)
}

module.exports = {
  find,
  findOne,
  create,
  update,
  remove
}
