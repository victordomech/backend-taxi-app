/* eslint-disable no-unused-vars */

const Car = require('../models/Car')
const Manager = require('../models/Manager')
const License = require('../models/License')
const Driver = require('../models/Driver')

const bcrypt = require('bcrypt')
const { guardIsSameId } = require('../common/guards')
const transaction = require('../common/transaction')

const find = (request, response) => {
  Manager.find({}).populate(['drivers', 'licenses', 'cars']).then(users => {
    response.json(users)
  })
}

const findOne = (request, response, next) => {
  const { id } = request.params

  Manager.findById(id).populate(['drivers', 'licenses', 'cars']).then(user => {
    if (user) return response.json(user)

    response.status(404).end()
  }).catch(err => {
    next(err)
  })
}

const create = async (request, response, next) => {
  const { body } = request
  const { email, password, name, surnames, phone } = body

  const saltRounds = 10
  const passwordHash = await bcrypt.hash(password, saltRounds)

  const newUser = new Manager({
    email,
    passwordHash,
    name,
    surnames,
    phone
  })

  transaction(request, response, next, async (session) => {
    try {
      const savedUser = await newUser.save({ session })
      return Promise.resolve(savedUser)
    } catch (err) {
      return Promise.reject(err)
    }
  })
}

const update = async (request, response, next) => {
  const { id } = request.params
  const { userId } = request
  guardIsSameId(id, userId)
  const { body } = request
  const { password, name, surnames, phone } = body

  const saltRounds = 10
  const passwordHash = await bcrypt.hash(password, saltRounds)

  const newUser = {
    passwordHash,
    name,
    surnames,
    phone
  }

  transaction(request, response, next, async (session) => {
    try {
      const result = await Manager.findByIdAndUpdate(id, newUser, { new: true })
      return Promise.resolve(result)
    } catch (err) {
      return Promise.reject(err)
    }
  })
}

const remove = (request, response, next) => {
  const { id } = request.params
  const { userId } = request
  guardIsSameId(id, userId)

  transaction(request, response, next, async (session) => {
    try {
      const result = await Manager.findByIdAndRemove(id, { session })
      await Driver.deleteMany({ manager: id })
      await License.deleteMany({ manager: id })
      await Car.deleteMany({ manager: id })
      //      await DaySheets.deleteMany({ manager: id })

      return Promise.resolve(result)
    } catch (err) {
      return Promise.reject(err)
    }
  }, true)
}

module.exports = {
  find,
  findOne,
  create,
  update,
  remove
}
