const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const { MANAGER } = require('../common/constants')

const Manager = require('../models/Manager')

const managerLogin = async (request, response) => {
  const { body } = request
  const { email, password } = body

  const manager = await Manager.findOne({ email })
  const isPasswordCorrect = manager === null ? false : await bcrypt.compare(password, manager.passwordHash)

  if (!isPasswordCorrect) {
    return response.status(401).json({
      error: 'invalid user or password'
    })
  }
  const userForToken = {
    id: manager._id,
    email: manager.email,
    name: manager.name,
    role: MANAGER
  }

  const token = jwt.sign(userForToken, process.env.SECRET, {
    expiresIn: 60 * 60 * 24 * 7
  })

  return response.send({
    email: manager.email,
    name: manager.name,
    surnames: manager.surnames,
    phone: manager.phone,
    drivers: manager.drivers,
    licenses: manager.licenses,
    cars: manager.cars,
    token
  })
}

module.exports = { managerLogin }
