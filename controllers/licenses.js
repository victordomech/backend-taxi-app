/* eslint-disable no-unused-vars */
const Driver = require('../models/Driver')
const Car = require('../models/Car')
const Manager = require('../models/Manager')
const License = require('../models/License')

const { guardIsItemOfManager } = require('../common/guards')
const transaction = require('../common/transaction')
const {
  findItemAndChange,
  findItemAndRemove,
  findManagerAndRemove,
  findManagerAndAdd
} = require('../common/findAndChange')

const find = (request, response, next) => {
  const { userId } = request
  const filter = { manager: userId }
  License.find(filter).populate('car').then(licenses => {
    response.json(licenses)
  }).catch(err => {
    next(err)
  })
}

const findOne = (request, response, next) => {
  const { id } = request.params
  const { userId } = request
  License.findById(id).populate('car').then(license => {
    if (license) {
      guardIsItemOfManager(userId, license)
      return response.json(license)
    }
    response.status(404).end()
  }).catch(err => {
    next(err)
  })
}

const create = async (request, response, next) => {
  const { licenseNumber, licenseType, car } = request.body
  const { userId } = request

  const manager = await Manager.findById(userId)

  const newLicense = new License({
    licenseNumber,
    licenseType,
    car,
    manager: manager.id
  })

  transaction(request, response, next, async (session) => {
    try {
      const savedLicense = await newLicense.save({ session })
      await findManagerAndAdd(manager, 'licenses', savedLicense.id, session)
      await findItemAndChange(car, 'license', savedLicense.id, Car, session)
      return Promise.resolve(savedLicense)
    } catch (err) {
      return Promise.reject(err)
    }
  })
}

const update = async (request, response, next) => {
  const { id } = request.params
  const { licenseNumber, licenseType } = request.body
  const { userId } = request

  const newLicense = new License({
    licenseNumber,
    licenseType,
    manager: userId
  })
  transaction(request, response, next, async (session) => {
    try {
      const result = await License.findByIdAndUpdate(id, newLicense, { new: true })
      if (result) {
        guardIsItemOfManager(userId, result)
      }
      return Promise.resolve(result)
    } catch (err) {
      return Promise.reject(err)
    }
  })
}

const remove = async (request, response, next) => {
  const { id } = request.params
  const { userId } = request

  transaction(request, response, next, async (session) => {
    try {
      const result = await License.findByIdAndRemove(id, { session })
      if (result) {
        guardIsItemOfManager(userId, result)
      }
      await findManagerAndRemove(result.manager, 'licenses', id, Manager, session)
      await findItemAndRemove(result?.car, 'license', Car, session)
      return Promise.resolve(result)
    } catch (err) {
      return Promise.reject(err)
    }
  }, true)
}

module.exports = {
  find,
  findOne,
  create,
  update,
  remove
}
