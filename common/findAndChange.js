
const findItemAndChange = async (item, atribute, id, model, session) => {
  try {
    if (item) {
      const itemToChange = await model.findById(item)
      itemToChange[atribute] = id
      await itemToChange.save({ session })
    } return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

const findItemAndRemove = async (item, atribute, model, session) => {
  try {
    if (item) {
      const itemToChange = await model.findById(item)

      itemToChange[atribute] = undefined
      await itemToChange.save({ session })
    } return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

const findArrayAndAdd = async (array, atribute, id, model, session) => {
  try {
    if (array) {
      for (const item of array) {
        const itemToChange = await model.findById(item)
        const found = itemToChange[atribute].find(itemID => itemID.toString() === id.toString())
        if (found !== -1) {
          itemToChange[atribute] = [...itemToChange[atribute], id]
          await itemToChange.save({ session })
        }
      }
    } return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

const findArrayAndRemove = async (array, atribute, id, model, session) => {
  try {
    if (array) {
      for (const item of array) {
        const itemToChange = await model.findById(item)
        const index = itemToChange[atribute].findIndex(itemID => itemID.toString() === id.toString())
        if (index !== -1) {
          itemToChange[atribute].splice(index, 1)
          await itemToChange.save({ session })
        }
      }
    } return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}
const findManagerAndAdd = async (manager, atribute, id, session) => {
  try {
    if (manager) {
      const arrayContent = manager[atribute]
      manager[atribute] = [...arrayContent, id]
      await manager.save({ session })
    } return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

const findManagerAndRemove = async (managerID, atribute, id, model, session) => {
  try {
    if (managerID) {
      const itemToChange = await model.findById(managerID)
      const index = itemToChange[atribute].findIndex(item => item.toString() === id.toString())
      if (index !== -1) {
        itemToChange.cars.splice(index, 1)
        await itemToChange.save({ session })
      }
    } return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

module.exports = { findItemAndChange, findArrayAndAdd, findItemAndRemove, findArrayAndRemove, findManagerAndRemove, findManagerAndAdd }
