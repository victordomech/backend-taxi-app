
const guardIsItemOfManager = (manager, item) => {
  if (Array.isArray(manager) && !manager.find(id => id.toString() !== item?.manager.toString())) {
    throw new Error('Unathorized')
  } else if (manager.toString() !== item?.manager.toString()) {
    throw new Error('Unathorized')
  }
}

const guardIsSameId = (id1, id2) => {
  if (id1.toString() !== id2.toString()) throw new Error('Unathorized')
}

module.exports = { guardIsItemOfManager, guardIsSameId }
