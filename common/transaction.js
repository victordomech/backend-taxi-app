const conn = require('../mongo')
const transaction = async (request, response, next, actions, remove = false) => {
  const session = await conn.startSession()
  try {
    session.startTransaction()
    const returnedResponse = await actions(session)
    await session.commitTransaction()
    session.endSession()
    if (!remove) {
      response.json(returnedResponse)
    }
    response.status(204).end()
  } catch (err) {
    await session.abortTransaction()
    session.endSession()
    next(err)
  }
}

module.exports = transaction
