const Yup = require('yup')

const createCarSchema = Yup.object({
  vehicleRegistration: Yup.string().required(),
  brand: Yup.string().required(),
  model: Yup.string().required()
})

module.exports = createCarSchema
