const Yup = require('yup')

const createLicenseSchema = Yup.object({
  licenseNumber: Yup.number().required()
})

module.exports = createLicenseSchema
