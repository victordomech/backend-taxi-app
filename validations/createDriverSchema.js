const Yup = require('yup')

const createDriverSchema = Yup.object({
  dni: Yup.string().required(),
  password: Yup.string().required(),
  name: Yup.string().required()
})

module.exports = createDriverSchema
