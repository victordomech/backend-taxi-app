const ERROR_HANDLERS = {

  CastError: res => res.status(400).send({ error: 'is used in malformed' }),

  ValidationError: (res, error) => res.status(409).send({ error: error.message }),

  JsonWebTokenError: res => res.status(401).json({ error: 'Token is missing or invalid' }),

  defaultError: res => res.status(500).end()
}

module.exports = (error, request, response, next) => {
  console.log('ERROR:::', error)
  const handler = ERROR_HANDLERS[error.name] || ERROR_HANDLERS.defaultError
  handler(response, error)
}
