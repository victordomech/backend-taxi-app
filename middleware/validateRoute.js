
const validateRoute = (schema) => async (req, res, next) => {
  try {
    await schema.validate(req.body)
    return next()
  } catch (err) {
    return next(err)
  }
}

module.exports = validateRoute
