const { MANAGER } = require('../common/constants')

module.exports = (request, response, next) => {
  const { role } = request

  if (role !== MANAGER) {
    return response.status(401).json({ error: 'unauthorized user' })
  }

  next()
}
