require('dotenv').config()
require('./mongo.js')

const express = require('express')
const app = express()
const cors = require('cors')
const notFound = require('./middleware/notFound')
const handleErrors = require('./middleware/handleError')
const indexRouter = require('./routes/index')
const apiRouter = require('./routes/api')

app.use(cors())
app.use(express.json())
app.use(express.static('images')) // Para archivos

// Route Prefixes
app.use('/', indexRouter)
app.use('/api', apiRouter)

app.use(notFound)

app.use(handleErrors)

const PORT = process.env.PORT
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})
