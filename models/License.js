const mongoose = require('mongoose')
const Manager = require('./Manager')
const Car = require('./Car')

const { model, Schema } = mongoose

const licenseSchema = new Schema({
  licenseNumber: { type: Number, required: true },
  licenseType: String,
  manager: {
    type: Schema.Types.ObjectId,
    ref: 'Manager'
  },
  car: {
    type: Schema.Types.ObjectId,
    ref: 'Car'
  }
})

licenseSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id
    delete returnedObject._id
    delete returnedObject.__v
  }
})

licenseSchema.post('findOneAndRemove', async doc => {
  if (doc) {
    const { manager: managerID, _id: licenseID, car: carID } = doc
    if (managerID) {
      await Manager.findById(managerID).then(async manager => {
        const licenseIndex = manager.licenses.findIndex(license => license.toString() === licenseID.toString()
        )
        if (licenseIndex !== -1) {
          manager.license.splice(licenseIndex, 1)
          await manager.save()
        }
      })
    }
    if (carID) {
      await Car.findById(carID).then(async car => {
        delete car.license
        await car.save()
      })
    }
  }
})
module.exports = model('License', licenseSchema)
