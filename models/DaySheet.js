const mongoose = require('mongoose')

const { model, Schema } = mongoose

const daySheetSchema = new Schema({
  startDateTime: { type: Date, required: true },
  endDateTime: Date,
  startInfo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Taximeter'
  },
  endInfo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Taximeter'
  },
  fuel: { type: Date, required: true },
  cardPayments: { type: Date, required: true },
  driverEarn: { type: Date, required: true },
  comment: String,
  image: { data: Buffer, contentType: String },
  driver: {
    type: Schema.Types.ObjectId,
    ref: 'Driver'
  },
  manager: {
    type: Schema.Types.ObjectId,
    ref: 'Manager'
  },
  license: {
    type: Schema.Types.ObjectId,
    ref: 'License'
  },
  car: {
    type: Schema.Types.ObjectId,
    ref: 'Car'
  }
})

daySheetSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id
    delete returnedObject._id
    delete returnedObject.__v
  }
})

module.exports = model('DaySheet', daySheetSchema)
