const mongoose = require('mongoose')

const { model, Schema } = mongoose

const managerSchema = new Schema({
  email: { type: String, required: true },
  passwordHash: { type: String, required: true },
  name: { type: String, required: true },
  surnames: String,
  phone: String,
  drivers: [{
    type: Schema.Types.ObjectId,
    ref: 'Driver'
  }],
  licenses: [{
    type: Schema.Types.ObjectId,
    ref: 'License'
  }],
  cars: [{
    type: Schema.Types.ObjectId,
    ref: 'Car'
  }],
  day_sheets: [{
    type: Schema.Types.ObjectId,
    ref: 'DaySheet'
  }]
})

managerSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id
    delete returnedObject._id
    delete returnedObject.__v
    delete returnedObject.passwordHash
  }
})

module.exports = model('Manager', managerSchema)
