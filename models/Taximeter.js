const mongoose = require('mongoose')

const { model, Schema } = mongoose

const taximeterSchema = new Schema({
  services: { type: Number, required: true },
  km_traveled: { type: Number, required: true },
  km_busy: { type: Number, required: true },
  km_car: { type: Number, required: true },
  Earnings: { type: Number, required: true }
})

module.exports = model('Taximeter', taximeterSchema)
