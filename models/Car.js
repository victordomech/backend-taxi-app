const mongoose = require('mongoose')

const { model, Schema } = mongoose

const carSchema = new Schema({
  vehicleRegistration: { type: String, required: true },
  brand: { type: String, required: true },
  model: { type: String, required: true },
  registrationDate: Date,
  fuelType: String,
  vin: String,
  insurancePolicy: String,
  manager: {
    type: Schema.Types.ObjectId,
    ref: 'Manager'
  },
  license: {
    type: Schema.Types.ObjectId,
    ref: 'License'
  },
  drivers: [{
    type: Schema.Types.ObjectId,
    ref: 'Driver'
  }],
  daySheets: [{
    type: Schema.Types.ObjectId,
    ref: 'DaySheet'
  }]
})

carSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id
    delete returnedObject._id
    delete returnedObject.__v
  }
})

module.exports = model('Car', carSchema)
