const mongoose = require('mongoose')

const Manager = require('./Manager')
const Car = require('./Car')

const { model, Schema } = mongoose

const driverSchema = new Schema({
  dni: { type: String, required: true },
  passwordHash: { type: String, required: true },
  name: { type: String, required: true },
  surnames: String,
  gender: String,
  phone: String,
  percentageProfit: Number,
  baseSalaryPerDay: Number,
  driverLicense: String,
  socialSecurityNumber: String,
  cars: [{
    type: Schema.Types.ObjectId,
    ref: 'Car'
  }],
  daySheets: [{
    type: Schema.Types.ObjectId,
    ref: 'DaySheet'
  }],
  manager: {
    type: Schema.Types.ObjectId,
    ref: 'Manager'
  }
})

driverSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id
    delete returnedObject._id
    delete returnedObject.__v
    delete returnedObject.passwordHash
  }
})

driverSchema.post('findOneAndRemove', async doc => {
  if (doc) {
    const { manager: managerID, _id: driverID, cars } = doc
    if (managerID) {
      await Manager.findById(managerID).then(async manager => {
        const driverIndex = manager.drivers.findIndex(driver => driver.toString() === driverID.toString()
        )
        if (driverIndex !== -1) {
          manager.drivers.splice(driverIndex, 1)
          await manager.save()
        }
      })
    }
    if (cars) {
      cars.map(async carID => {
        await Car.findById(carID).then(async car => {
          const carIndex = car.drivers.findIndex(driver => driver.toString() === driverID.toString()
          )
          if (carIndex !== -1) {
            car.drivers.splice(carIndex, 1)
            await car.save()
          }
        })
      })
    }
  }
})

module.exports = model('Driver', driverSchema)
