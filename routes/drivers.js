const express = require('express')
const driversController = require('../controllers/drivers')
const userExtractor = require('../middleware/userExtractor')
const validateRoute = require('../middleware/validateRoute')
const createDriverSchema = require('../validations/createDriverSchema')

const router = express.Router()

router.get('/', userExtractor, driversController.find)
router.get('/:id', userExtractor, driversController.findOne)
router.post('/', userExtractor, validateRoute(createDriverSchema), driversController.create)
// router.put('/:id', userExtractor, validateRoute(createDriverSchema), driversController.update)
router.delete('/:id', userExtractor, driversController.remove)

module.exports = router
