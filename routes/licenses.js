const express = require('express')
const licenseController = require('../controllers/licenses')
const userExtractor = require('../middleware/userExtractor')
const validateRoute = require('../middleware/validateRoute')
const createLicenseSchema = require('../validations/createLicenseSchema')

const router = express.Router()

router.get('/', userExtractor, licenseController.find)
router.get('/:id', userExtractor, licenseController.findOne)
router.post('/', userExtractor, validateRoute(createLicenseSchema), licenseController.create)
// router.put('/:id', userExtractor, licenseController.update)
router.delete('/:id', userExtractor, licenseController.remove)

module.exports = router
