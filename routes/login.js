const express = require('express')
const loginController = require('../controllers/login')

const router = express.Router()

router.post('/manager', loginController.managerLogin)

module.exports = router
