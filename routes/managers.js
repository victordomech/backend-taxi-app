const express = require('express')
const managersController = require('../controllers/managers')
const userExtractor = require('../middleware/userExtractor')

const router = express.Router()

// router.get('/', userExtractor, managersController.find)
// router.get('/:id', userExtractor, managersController.findOne)
router.post('/', managersController.create)
router.put('/:id', userExtractor, managersController.update)
router.delete('/:id', userExtractor, managersController.remove)

module.exports = router
