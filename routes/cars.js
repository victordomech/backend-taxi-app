const express = require('express')
const carsController = require('../controllers/cars')
const userExtractor = require('../middleware/userExtractor')
const validateRoute = require('../middleware/validateRoute')
const createCarSchema = require('../validations/createCarSchema')

const router = express.Router()

router.get('/', userExtractor, carsController.find)
router.get('/:id', userExtractor, carsController.findOne)
router.post('/', userExtractor, validateRoute(createCarSchema), carsController.create)
// router.put('/:id', userExtractor, validateRoute(createCarSchema), carsController.update)
router.delete('/:id', userExtractor, carsController.remove)

module.exports = router
