const app = require('express').Router()
const loginRouter = require('./login')
const managersRouter = require('./managers')
const carsRouter = require('./cars')
const driversRouter = require('./drivers')
const licensesRouter = require('./licenses')
const daySheetRouter = require('./daySheets')

app.use('/login', loginRouter)
app.use('/managers', managersRouter)
app.use('/cars', carsRouter)
app.use('/drivers', driversRouter)
app.use('/licenses', licensesRouter)
app.use('/daySheets', daySheetRouter)

module.exports = app
